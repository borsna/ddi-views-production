<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://www.omg.org/spec/UML/20110701" 
	xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    exclude-result-prefixes="ddifunc"
    version="2.0">

    <!-- imports -->
	<xsl:import href="flattening.xsl"/>
	
    <!-- options -->
    <xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="xmi:XMI">
		<xmi:XMI xmi:version="2.4.1">
			<xsl:attribute name="xmi:version">
				<xsl:value-of select="xmi:version"/>
			</xsl:attribute>
			<xsl:copy-of select="xmi:Documentation"/>
			<xsl:apply-templates select="uml:Model"/>
			<xsl:apply-templates select="xmi:Extension"/>
		</xmi:XMI>
	</xsl:template>
	
	<xsl:template match="uml:Model">
		<uml:Model>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="packagedElement[1]" mode="libraryToXMI"/>
			<xsl:copy-of select="packagedElement[2]"/>
		</uml:Model>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="libraryToXMI">
		<packagedElement>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="packagedElement" mode="packageToXMI"/>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="packageToXMI">
		<packagedElement>
			<xsl:copy-of select="@*"/>
			<!--<xsl:apply-templates select="packagedElement[@xmi:type='uml:Class' and @isAbstract='true']" mode="absractClassToXMI"/>-->
			<xsl:apply-templates select="packagedElement[@xmi:type='uml:Class' and not(@isAbstract='true')]" mode="classToXMI"/>
			<xsl:copy-of select="packagedElement[@xmi:type='uml:Enumeration' or @xmi:type='uml:DataType']"/>
		</packagedElement>
	</xsl:template>

</xsl:stylesheet>